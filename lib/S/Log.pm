package S::Log;

use strict;
use vars qw ( @ISA $VERSION );

use Fcntl ':flock';
use IO::Handle;
use Carp;

BEGIN {
  $VERSION = '0.01';
}
sub VERSION { $VERSION }

sub new
{
  my ($class, %opt) = @_;

  my $path  = delete $opt{path} || _default_path();

  # This modified line of code from LWP
  carp("Unrecognized S::Log options: @{[sort keys %opt]}") if %opt;

  my $self = bless {
    path  => $path,
    handle => S::Log->_handle($path),
  }, $class;
  return $self;
}

# Subroutine from Mojo::Log
sub format
{
  my ($self, $level, @msgs) = @_;
  return '[' . localtime(time) . "] [$level] " . join("\n", @msgs) . "\n";
}

sub log
{
  my $self  = shift;
  my $level = uc shift;
  my $handle = $self->{handle};
  flock $handle, LOCK_EX or croak $!;
  croak "Can't write to log: $!"
    unless defined $handle->syswrite($self->format($level, @_));
  flock $handle, LOCK_UN or croak $!;
} 

# This group of subroutines from Mojo::Log
sub debug { shift->log(debug => @_) };
sub info  { shift->log(info => @_) };
sub warn  { shift->log(warn => @_) };
sub error { shift->log(error => @_) };
sub fatal { shift->log(fatal => @_) };

sub _handle
{
  my ($self, $path) = (shift, shift);
  Carp::croak qq/Can't open log file "$path": $!/
    unless open my $file, '>>:utf8', $path;
  return $file;
}

# This subroutine from App::Ack
sub _default_path
{
  require File::Basename;
  return File::Basename::basename( $0 ).'.log';
}

1;

=head1 NAME

S::Log - Very simple logger to file.

=head1 SYNOPSIS

  use S::Log;

  # Log to 'script.pl.log'
  my $log = S::Log->new;

  # Customize log file location
  my $log = S::Log->new(path => '/var/log/filename.log');

  # Log messages
  $log->debug("Why isn't this working?");
  $log->info("FYI: it happened again");
  $log->warn("This might be a problem");
  $log->error("Garden variety error");
  $log->fatal("Boom!");
  $log->log("my new level", "You wonna do what you wonna do") ;

=head1 DESCRIPTION

L<S::Log> is a very simple logger to file. Code and idea based
on L<Mojo::Log>.

=head1 ATTRIBUTES

L<S::Log> implements the following attributes.

=head2 C<level>

These levels are currently available:

=over 2

=item C<debug>

=item C<info>

=item C<warn>

=item C<error>

=item C<fatal>

=back

=head2 C<path>

  my $log = S::Log->new(path => "/path/to/log.file");                 1

Default log file placed in current script directory and named as
C<scriptname> with added C<.log> at the end.

Log file path used by C<_handle>.

=head1 METHODS

=head2 C<new>

  my $log = S::Log->new;

Construct a new L<S::Log> object and subscribe to C<message> event with
default logger.

=head2 C<format>

  $log = $log->format(debug => 'You screwed up, but that is ok');

Format log message.

=head2 C<debug>

  $log->debug('You screwed up, but that is ok');

Log debug message.

=head2 C<error>

  $log->error('You really screwed up this time');

Log error message.

=head2 C<fatal>

  $log->fatal('Its over...');

Log fatal message.

=head2 C<info>

  $log->info('You are bad, but you prolly know already');

Log info message.

=head2 C<log>

  $log->log('debug' 'This should work');

Emit C<message> event.

=head2 C<warn>

  $log->warn('Dont do that Dave...');

Log warn message.

=head1 LICENSE

Copyright (c) 2012 Alexander Groschev <sattellite@yandex.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


=cut