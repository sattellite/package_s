use Test::Simple 'no_plan';
use File::Spec::Functions 'catdir';
use File::Temp 'tempdir';
use S::Log; # What i'm testing

my $dir  = tempdir CLEANUP => 1;
my $path = catdir $dir, 'test.log';
my $log  = S::Log->new(path => $path);
ok( defined($log) && ref $log eq 'S::Log', 'new() works');
ok( $log->{path} eq $path, 'change path works');
$log->debug('Just works.');
open my $fh, '<:utf8', $path;
my $data = <$fh>;
close $fh;
ok( $data =~ qr/^\[.*\] \[DEBUG\] Just works\.\n$/, 'right content');