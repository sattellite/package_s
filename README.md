# ABOUT

## NAME

S::Log - Very simple logger to file.

## SYNOPSIS

    use S::Log;

    # Log to 'script.pl.log'
    my $log = S::Log->new;
 
    # Customize log file location
    my $log = S::Log->new(path => '/var/log/filename.log');
 
    # Log messages
    $log->debug("Why isn't this working?");
    $log->info("FYI: it happened again");
    $log->warn("This might be a problem");
    $log->error("Garden variety error");
    $log->fatal("Boom!");
    $log->log("my new level", "You wonna do what you wonna  do") ;

## DESCRIPTION

`S::Log` is a very simple logger to file. Code and idea based on `Mojo::Log`.

## ATTRIBUTES

`S::Log` implements the following attributes.

 * level

These levels are currently available:

 * debug
 * info
 * warn
 * error
 * fatal

## path

    my $log = S::Log->new(path => "/path/to/log.file");                 1

Default log file placed in current script directory and named as `scriptname` with added `.log` at the end.

Log file path used by `_handle`.

## METHODS

### new

    my $log = S::Log->new;

Construct a new `S::Log` object and subscribe to `message` event with default logger.

### format

    $log = $log->format(debug => 'You screwed up, but that is ok');

Format log message.

### debug

    $log->debug('You screwed up, but that is ok');

Log debug message.

### error

    $log->error('You really screwed up this time');

Log error message.

### fatal

    $log->fatal('Its over...');

Log fatal message.

### info

    $log->info('You are bad, but you prolly know already');

Log info message.

### log

    $log->log('debug' 'This should work');

Emit `message` event.

### warn

    $log->warn('Dont do that Dave...');

Log warn message.

## LICENSE

MIT License

Copyright (c) 2012 Alexander Groschev <sattellite@yandex.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# FAST INSTALL

    cpanm https://bitbucket.org/sattellite/package_s/downloads/S-Log-0.01.tar.gz